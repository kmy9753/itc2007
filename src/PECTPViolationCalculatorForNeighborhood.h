#include "PECTPViolationCalculator.h"

#ifndef PECTP_VIOLATION_CALCULATOR_FOR_NEIGHBORHOOD_H
#define PECTP_VIOLATION_CALCULATOR_FOR_NEIGHBORHOOD_H

class PECTPViolationCalculatorForNeighborhood : public PECTPViolationCalculator{
private:
    PECTPSolution* solution;

public:
    PECTPViolationCalculatorForNeighborhood(PECTPSolution* solution){
        this->solution = solution;
    }

    int calculateDeltaHardViolationAfterME(int, int, int);
    int calculateDeltaSoftViolationAfterME(int, int, int);
    int calculateDeltaDistanceToFeasibilityAfterME(int, int, int);
};

#endif
