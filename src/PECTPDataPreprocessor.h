#include <vector>

#include "PECTPDatasetReceiver.h"

#ifndef PECTP_DATASET_PREPROCESSOR_H
#define PECTP_DATASET_PREPROCESSOR_H

class PECTPDataPreprocessor{
private:
    PECTPDatasetReceiver *dr;

    void preprocess(void); //carry over functions named make*

    void makeRoomCharmOrder(void);
    void makeEnrolmentedStudentNum(void);
    void makeRelationBetER(void);
    void makeRelationBetEE(void);
    void makeOneRoomEvent(void);
    void makeAvailableTimeslot(void);
    void makeAllRoomEvent(void);

    int countStudentNum(int);

    PECTPDataPreprocessor(){
        dr = dr->getInstance();

        relationBetER = std::vector< std::vector<bool> >(dr->eventNum, std::vector<bool>(dr->roomNum, true));
        relationBetEE = std::vector< std::vector<bool> >(dr->eventNum, std::vector<bool>(dr->eventNum, true));
        oneRoomEvent.resize(dr->eventNum);
        roomCharmOrder.resize(dr->roomNum);
        enrolmentedStudentsNum.resize(dr->eventNum);
        availableTimeslot = std::vector< std::vector<int> >(dr->eventNum);
        allRoomEvent.resize(dr->eventNum);
    }

    static PECTPDataPreprocessor* instance;

public:
    static const int NONE = -1;

    std::vector< std::vector<bool> > relationBetER,       //true:OK/false:NG
                                     relationBetEE;       //true:OK/false:NG
    std::vector<int> oneRoomEvent,                   //value:an only assignable room
                     roomCharmOrder,                 //value:room index (ascending order)
                     enrolmentedStudentsNum;         //value:the number of students who enrolment the event
    std::vector< std::vector<int> > availableTimeslot;    //value:timeslot index; for ith event
    std::vector<bool> allRoomEvent;
    
    static PECTPDataPreprocessor* getInstance(){
        if(instance == 0){
            instance = new PECTPDataPreprocessor();

            instance->preprocess();
        }

        return instance;
    }
};

#endif
