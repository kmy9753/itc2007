#include "PECTPGASolver.h"
#include "PECTPViolationCalculator.h"
#include <cstdlib>
#include <algorithm>

#include "PECTPTester.h"

using namespace std;

PECTPSolution* PECTPGASolver::crossover(PECTPSolution* p1, PECTPSolution* p2){
    for(int e = 0; e < dr->eventNum; e++) assigned[e] = false;

    runPreprocessOfCrossover(p1, p2);

    PECTPSolution* offspring = new PECTPSolution();

    crossoverBetOneRoomEvents(p1, p2, offspring);
    crossoverBetRegularRoomEvents(p1, p2, offspring);
    crossoverBetAllRoomEvents(p1, p2, offspring);

    return offspring;
}

void PECTPGASolver::runPreprocessOfCrossover(PECTPSolution* p1, PECTPSolution* p2){}

void PECTPGASolver::crossoverBetNotAllRoomEvent(PECTPSolution* p1, PECTPSolution* p2, PECTPSolution* offspring, int e){
    PECTPSolution* tarP = (rand() % 2) ? p1:p2;
    PECTPSolution* anotherP = (tarP == p1) ? p2:p1;

    if(tarP->getTimeslot(e) == PECTPSolution::NONE){
        swap(tarP, anotherP);
    }

//int cnt = 0;
    bool conflicted;
    do{
        conflicted = false;

        pair<int, int> gene = tarP->get(e);

        offspring->set(e, gene);
        assigned[e] = true;
        if(gene.first == PECTPSolution::NONE) return;

        for(int confE = 0; confE < p1->size(); confE++){
            if(assigned[confE]) continue;
            if(anotherP->get(confE) == gene){
                conflicted = true;
                e = confE;

                break;
            }
        }
//if(conflicted) {
//    cnt++;
//}       
    } while(conflicted);
//cout << "conflicted num: " << cnt << endl;
}

void PECTPGASolver::crossoverBetOneRoomEvents(PECTPSolution* p1, PECTPSolution* p2, PECTPSolution* offspring){
    for(int e = 0; e < p1->size(); e++){
        if(dp->oneRoomEvent[e] == PECTPDataPreprocessor::NONE ||
           assigned[e]) continue;
     
        crossoverBetNotAllRoomEvent(p1, p2, offspring, e);
    }
}

void PECTPGASolver::crossoverBetRegularRoomEvents(PECTPSolution* p1, PECTPSolution* p2, PECTPSolution* offspring){
    for(int e = 0; e < p1->size(); e++){
        if(dp->oneRoomEvent[e] != PECTPDataPreprocessor::NONE ||
           dp->allRoomEvent[e] ||
           assigned[e]) continue;
        
        crossoverBetNotAllRoomEvent(p1, p2, offspring, e);
    }
}

void PECTPGASolver::crossoverBetAllRoomEvents(PECTPSolution* p1, PECTPSolution* p2, PECTPSolution* offspring){
    for(int e = 0; e < p1->size(); e++){
        if(!dp->allRoomEvent[e] ||
           assigned[e]) continue;
        
        if(rand() % 2) swap(p1, p2);

        for(int i = 0; i < 2; i++){
            if(p1->getTimeslot(e) != PECTPSolution::NONE && 
               offspring->countEvent(p1->getTimeslot(e)) < dr->roomNum){
                offspring->set(e, p1->get(e));
                assigned[e] = true;

                break;
            }
            swap(p1, p2);
        }

        if(!assigned[e]){
            for(int t = 0; t < dr->timeslotNum; t++){
                if(offspring->countEvent(t) < dr->roomNum){
                    offspring->set(e, make_pair(t, PECTPSolution::NONE));
                    assigned[e] = true;

                    break;
                }
            }
        }
    }
}

PECTPSolution* PECTPGASolver::select(void){
    return selectWithTournamentSelection((int)DEFAULT_TOURNAMENT_SIZE);
}

int PECTPGASolver::selectBadIndividualIndex(void){
    int ret;
    int aceInd = min_element(fitnesses.begin(), fitnesses.end()) - fitnesses.begin();

    while((ret = rand() % individuals.size()) == aceInd);

    return ret;
}

PECTPSolution* PECTPGASolver::selectWithTournamentSelection(int tournamentSize){
    int minInd = rand() % individuals.size();

    for(int i = 0; i < tournamentSize - 1; i++){
        int ind = rand() % individuals.size();

        if(fitnesses[ind] <= fitnesses[minInd]){
            minInd = ind;
        }
    }

    return individuals[minInd];
}

void PECTPGASolver::mutate(PECTPSolution* originIndividual){
}

void PECTPGASolver::run(void){
    run(DEFAULT_TRIAL_NUM);
}

void PECTPGASolver::run(int trialNum){
    PECTPViolationCalculator calculator;
    vector<PECTPSolution*> nextIndividuals(individuals.size());

    PECTPTester* tester = PECTPTester::getInstance();

    while(trialNum--){

        for(int i = 0; i < individuals.size(); i++){
            fitnesses[i] = calculator.calculateHardViolation(*individuals[i]);
                         + calculator.calculateSoftViolation(*individuals[i])
                         + calculator.calculateDistanceToFeasibility(*individuals[i]);
        }

        for(int i = 0; i < individuals.size(); i++){
            PECTPSolution* selectedIndividual = select();
            PECTPSolution* nextIndividual;

            //crossover
            double prob = (double)rand() / RAND_MAX;
            if(prob <= crossoverProbability){
                PECTPSolution* partner;
                while(selectedIndividual == (partner = select()));

                nextIndividual = crossover(selectedIndividual, partner);
            }

            else{
                nextIndividual = new PECTPSolution(*selectedIndividual);
            }

            //mutation
            prob = (double)rand() / RAND_MAX;
            if(prob <= mutationProbability){
                mutate(nextIndividual);
            }

            nextIndividuals[i] = nextIndividual;
        }

        for(int i = 0; i < individuals.size(); i++){
            delete individuals[i];
        }

        individuals = nextIndividuals;
    }

    for(int i = 0; i < individuals.size(); i++){
        fitnesses[i] = calculator.calculateHardViolation(*individuals[i]);
                     + calculator.calculateSoftViolation(*individuals[i])
                     + calculator.calculateDistanceToFeasibility(*individuals[i]);
    }

    tester->output(*getBestIndividual());
}

void PECTPGASolver::setIndividual(PECTPSolution* solution){
    int ind = selectBadIndividualIndex();

    setIndividual(solution, ind);
}

void PECTPGASolver::setIndividual(PECTPSolution* solution, int ind){
    if(ind < 0 || individuals.size() <= ind){
        return;
    }

    if(individuals[ind] != NULL) delete individuals[ind];
    individuals[ind] = solution;
}

PECTPSolution* PECTPGASolver::getIndividual(int ind){
    return individuals[ind];
}

PECTPSolution* PECTPGASolver::getBestIndividual(void){
    return individuals[min_element(fitnesses.begin(), fitnesses.end()) - fitnesses.begin()];
}

PECTPSolution* PECTPGASolver::getBadIndividual(void){
    return individuals[selectBadIndividualIndex()];
}

