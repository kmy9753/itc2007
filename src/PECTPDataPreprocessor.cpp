#include <algorithm>

#include"PECTPDataPreprocessor.h"

using namespace std;

PECTPDataPreprocessor* PECTPDataPreprocessor::instance = 0;

void PECTPDataPreprocessor::preprocess(void){
    
    //make relationBetER
    makeRelationBetER();

    //make oneRoomEvent
    makeOneRoomEvent();

    //make relationBetEE
    makeRelationBetEE();

    //make roomCharmOrder
    makeRoomCharmOrder();

    //make enrolmentedStudentsNum
    makeEnrolmentedStudentNum();

    //make availableTimeslot
    makeAvailableTimeslot();

    //make allRoomEvent
    makeAllRoomEvent();
}

int PECTPDataPreprocessor::countStudentNum(int event){
    int cnt = 0;

    for(int s = 0; s < dr->studentNum; s++){
        if(dr->relationBetSE[s][event]) cnt++;
    }

    return cnt;
}

void PECTPDataPreprocessor::makeEnrolmentedStudentNum(void){
    enrolmentedStudentsNum.clear();

    for(int e = 0; e < dr->eventNum; e++){
        for(int s = 0; s < dr->studentNum; s++){
            if(dr->relationBetSE[s][e]){
                enrolmentedStudentsNum[e]++;
            }
        }
    }
}

void PECTPDataPreprocessor::makeRelationBetER(void){
    for(int e = 0; e < dr->eventNum; e++){
        for(int r = 0; r < dr->roomNum; r++){

            //cap. over
            if(dr->roomCap[r] < countStudentNum(e)){
                relationBetER[e][r] = false;
                continue;
            }

            //feature not satisfied
            for(int f = 0; f < dr->featureNum; f++){
                if(dr->relationBetEF[e][f] && !dr->relationBetRF[r][f]){
                    relationBetER[e][r] = false;
                }
            }
        }
    }
}

void PECTPDataPreprocessor::makeRoomCharmOrder(void){
    vector<int> assignableEventNum(dr->roomNum);

    for(int r = 0; r < dr->roomNum; r++){
        for(int e = 0; e < dr->eventNum; e++){
            if(relationBetER[e][r]){
                assignableEventNum[r]++;
            }
        }
    }

    for(int i = 0; i < dr->roomNum; i++){
        int minElementInd = min_element(assignableEventNum.begin(), assignableEventNum.end()) - assignableEventNum.begin();
        roomCharmOrder[i] = minElementInd;
        assignableEventNum[minElementInd] = dr->eventNum + 1;
    }
}

void PECTPDataPreprocessor::makeRelationBetEE(void){
    for(int e1 = 0; e1 < dr->eventNum; e1++){
        for(int e2 = 0; e2 < dr->eventNum; e2++){
            
            //conflict room
            if(oneRoomEvent[e1] != NONE && oneRoomEvent[e1] == oneRoomEvent[e2]){
                relationBetEE[e1][e2] = false;
                continue;
            }

            //conflict student
            for(int s = 0; s < dr->studentNum; s++){
                if(dr->relationBetSE[s][e1] && dr->relationBetSE[s][e2]){
                    relationBetEE[e1][e2] = false;
                    break;
                }
            }
        }
    }
}

void PECTPDataPreprocessor::makeOneRoomEvent(void){
    for(int e = 0; e < dr->eventNum; e++){
        if(count(relationBetER[e].begin(), relationBetER[e].end(), true) == 1){
            oneRoomEvent[e] = find(relationBetER[e].begin(), relationBetER[e].end(), true) - relationBetER[e].begin();
        }
        else oneRoomEvent[e] = NONE;
    }
}

void PECTPDataPreprocessor::makeAvailableTimeslot(void){
    for(int e = 0; e < dr->eventNum; e++){
        for(int t = 0; t < dr->timeslotNum; t++){
            if(dr->relationBetET[e][t]){
                availableTimeslot[e].push_back(t);
            }
        }
    }
}

void PECTPDataPreprocessor::makeAllRoomEvent(void){
    for(int e = 0; e < dr->eventNum; e++){
        allRoomEvent[e] = (count(relationBetER[e].begin(),
                           relationBetER[e].end(), true) == dr->roomNum);
    }
}
