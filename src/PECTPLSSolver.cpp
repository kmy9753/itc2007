#include <cstdlib>
#include <iostream> //for debug

#include "PECTPLSSolver.h"

#include "PECTPTester.h"

using namespace std;

void PECTPLSSolver::run(void){
    run(DEFAULT_TRIAL_NUM);
}

void PECTPLSSolver::run(int trialNum){
    int originTrialNum = trialNum;
    coefHardViolation = 1.;

    PECTPTester* tester = PECTPTester::getInstance();

    while(trialNum--){
//if(!(trialNum % 100))cout << originTrialNum - trialNum - 1 << " " << solution->getHardViolation() << " " << solution->getSoftViolation() << endl;
        if(trialNum == originTrialNum / 2){
            coefHardViolation *= 2;
            evaluationValue = (int)(coefHardViolation * solution->getHardViolation())
                                  + solution->getDistanceToFeasibility()
                                  + solution->getSoftViolation();
        }

        int op = rand() % 2;
        int tarEvent = selectTargetEvent();

        switch(op){
        case 0:
        {
            for(int t = rand() % (dr->timeslotNum + 1), tCnt = 0; tCnt < dr->timeslotNum; t = (t + 1) % (dr->timeslotNum + 1), tCnt++){
                if(t == dr->timeslotNum) t = PECTPSolution::NONE;
                else if(!dr->relationBetET[tarEvent][t])
                if(t == solution->getTimeslot(tarEvent)) continue;

                int preT = solution->getTimeslot(tarEvent);
                if(neighborhoodManager.moveEvent(tarEvent, t, solution)){
                    if(updateSolution()){
                        //cout << endl << "--------restTrialNum = " << trialNum << "----------" << endl;

                        break;
                    }
                }
            }

            break;
        }

        case 1:
        {
            for(int e = selectTargetEvent(), eCnt = 0; eCnt < dr->eventNum; e = (e + 1) % dr->eventNum, eCnt++){
                if(e == tarEvent || solution->getTimeslot(e) == solution->getTimeslot(tarEvent)) continue;

                if(neighborhoodManager.swapEvent(tarEvent, e, solution)){
                    if(updateSolution()){
                        //cout << endl << "--------restTrialNum = " << trialNum << "----------" << endl;

                        break;
                    }
                }
            }

            break;
        }
        default:
            break;
        }
        if((trialNum % (originTrialNum / 100)) == 0) tester->output(*solution);
    }
}

bool PECTPLSSolver::updateSolution(void){
    int nextHardViolation = solution->getHardViolation();
    int nextSoftViolation = solution->getSoftViolation();
    int nextDistanceToFeasibility = solution->getDistanceToFeasibility();
    int nextEvaluationValue = (int)(coefHardViolation * nextHardViolation) 
                            + nextSoftViolation
                            + nextDistanceToFeasibility;

    if(evaluationValue < nextEvaluationValue){
        neighborhoodManager.undo();

        return false;
    }
    else{
        evaluationValue = nextEvaluationValue;
        //cout << "------------------------------------" << endl;
        //cout << endl << "HardViolation: " << nextHardViolation << endl;
        //cout << "SoftViolation: " << nextSoftViolation << endl;

        //cout << endl;
        //for(int e = 0; e < dr->eventNum; e++){
        //    cout << e << "th event: t = " << solution->getTimeslot(e) << ", r = " << solution->getRoom(e) << endl;
        //}

        return true;
    }
}

int PECTPLSSolver::selectTargetEvent(void){
    return curEvent = (++curEvent) % dr->eventNum;
}
