#include "PECTPDatasetReceiver.h"
#include "PECTPDataPreprocessor.h"
#include "PECTPSolution.h"

#ifndef PECTP_SOLUTION_FACTORY_H
#define PECTP_SOLUTION_FACTORY_H

class PECTPSolutionFactory{
private:
    PECTPDatasetReceiver* dr;
    PECTPDataPreprocessor* dp;
    int defaultEventDelta;
    int defaultEvent;

    void init(void){
        defaultEvent =(defaultEvent + defaultEventDelta) % dr->eventNum;
    }

    PECTPSolution* createByI0(void);
    PECTPSolution* createByI1(void);

    int decideRoom(int, int&, PECTPSolution*);

public:
    PECTPSolutionFactory(int defaultEvent = 0, int defaultEventDelta = 0)
    : dr(PECTPDatasetReceiver::getInstance()),
      dp(PECTPDataPreprocessor::getInstance()){
        this->defaultEvent = defaultEvent;
        this->defaultEventDelta = defaultEventDelta;
        this->dr = dr;
        this->dp = dp;
    }

    PECTPSolution* create(void);
};

#endif

