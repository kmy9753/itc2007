#include <iostream>

#include"PECTPDatasetReceiver.h"

using namespace std;

PECTPDatasetReceiver* PECTPDatasetReceiver::instance = 0;

void PECTPDatasetReceiver::receive(void){
    cin >> eventNum >> roomNum >> featureNum >> studentNum;

    roomCap.resize(roomNum);
    relationBetSE = vector< vector<bool> >(studentNum, vector<bool>(eventNum, false));
    relationBetRF = vector< vector<bool> >(roomNum, vector<bool>(featureNum, false));
    relationBetEF = vector< vector<bool> >(eventNum, vector<bool>(featureNum, false));
    relationBetET = vector< vector<bool> >(eventNum, vector<bool>(timeslotNum, false));
    relationBetEE = vector< vector<int> >(eventNum, vector<int>(eventNum, 0));
    
    for(int r = 0; r < roomNum; r++){
        cin >> roomCap[r];
    }
    for(int s = 0; s < studentNum; s++){
        for(int e = 0; e < eventNum; e++){
            int tmp; cin >> tmp;
            relationBetSE[s][e] = tmp;
        }
    }
    for(int r = 0; r < roomNum; r++){
        for(int f = 0; f < featureNum; f++){
            int tmp; cin >> tmp;
            relationBetRF[r][f] = tmp;
        }
    }
    for(int e = 0; e < eventNum; e++){
        for(int f = 0; f < featureNum; f++){
            int tmp; cin >> tmp;
            relationBetEF[e][f] = tmp;
        }
    }
    for(int e = 0; e < eventNum; e++){
        for(int t = 0; t < timeslotNum; t++){
            int tmp; cin >> tmp;
            relationBetET[e][t] = tmp;
        }
    }
    for(int e1 = 0; e1 < eventNum; e1++){
        for(int e2 = 0; e2 < eventNum; e2++){
            cin >> relationBetEE[e1][e2];
        }
    }
}

