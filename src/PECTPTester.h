#include <cstdio>

#include "PECTPSmartSolution.h"
#include "PECTPDatasetReceiver.h"
#include "PECTPDataPreprocessor.h"
#include "PECTPViolationCalculator.h"

#ifndef PECTP_TESTER_H
#define PECTP_TESTER_H

class PECTPTester{
private:
    PECTPDataPreprocessor *dp;
    PECTPDatasetReceiver *dr;
    PECTPViolationCalculator vc;

    PECTPTester(){
        dp = PECTPDataPreprocessor::getInstance();
        dr = PECTPDatasetReceiver::getInstance();
        turnCnt = 0;
    }

    static PECTPTester* instance;

public:
    int turnCnt;

    void output(PECTPSolution& solution){
        int distanceToFeasibilityRate = 0;
        for(int e = 0; e < dr->eventNum; e++){
            if(solution.getTimeslot(e) == PECTPSolution::NONE){
                distanceToFeasibilityRate++;
            }
        }
        distanceToFeasibilityRate *= 100;
        distanceToFeasibilityRate /= dr->eventNum;

        printf("%6d %6d %6d %3d %6d\n", ++turnCnt, vc.calculateHardViolation(solution), vc.calculateSoftViolation(solution), 100 - distanceToFeasibilityRate, vc.calculateDistanceToFeasibility(solution));
    }

    void output(PECTPSmartSolution& solution){
        int distanceToFeasibilityRate = 0;
        for(int e = 0; e < dr->eventNum; e++){
            if(solution.getTimeslot(e) == PECTPSolution::NONE){
                distanceToFeasibilityRate++;
            }
        }
        distanceToFeasibilityRate *= 100;
        distanceToFeasibilityRate /= dr->eventNum;

        printf("%6d %6d %6d %3d %6d\n", ++turnCnt, solution.getHardViolation(), solution.getSoftViolation(), 100 - distanceToFeasibilityRate, solution.getDistanceToFeasibility());
    }

    static PECTPTester* getInstance(void){
        if(instance == 0){
            instance = new PECTPTester();
        }

        return instance;
    }
};

#endif

