#include "PECTPProbabilisticGASolver.h"

using namespace std;

void PECTPProbabilisticGASolver::runPreprocessOfCrossover(PECTPSolution* p1, PECTPSolution* p2){
    for(int e = 0; e < dr->eventNum; e++){
        if(p1->get(e) != p2->get(e))
            probVec->increment(e);
    }
}

