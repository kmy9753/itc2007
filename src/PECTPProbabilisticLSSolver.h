#include "PECTPLSSolver.h"
#include "PECTPProbabilityVector.h"

#ifndef PECTP_PROBABILISTIC_LS_SOLVER_H
#define PECTP_PROBABILISTIC_LS_SOLVER_H

class PECTPProbabilisticLSSolver : public PECTPLSSolver{
private:
    PECTPProbabilityVector* probVec;

public:
    PECTPProbabilisticLSSolver(PECTPProbabilityVector* probVec){
        this->probVec = probVec;
    }
};

#endif

