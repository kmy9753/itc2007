#include "PECTPGASolver.h"
#include "PECTPProbabilityVector.h"

#ifndef PECTP_PROBABILISTIC_GA_SOLVER_H
#define PECTP_PROBABILISTIC_GA_SOLVER_H

class PECTPProbabilisticGASolver : public PECTPGASolver{
private:
    PECTPProbabilityVector* probVec;
    void runPreprocessOfCrossover(PECTPSolution*, PECTPSolution*);

public:
    PECTPProbabilisticGASolver(PECTPProbabilityVector* probVec, int individualsNum = DEFAULT_INDIVIDUALS_NUM)
    : PECTPGASolver(individualsNum){
        this->probVec = probVec;
    }
};

#endif
