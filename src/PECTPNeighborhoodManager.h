#include <stack>
#include <vector>
#include <utility>

#include "PECTPDatasetReceiver.h"
#include "PECTPDataPreprocessor.h"
#include "PECTPSolution.h"

#ifndef PECTP_NEIGHBORHOOD_MANAGER_H
#define PECTP_NEIGHBORHOOD_MANAGER_H

class PECTPNeighborhoodManager{
private:
    typedef enum{
        MOVE_EVENT,
        SWAP_EVENT
    } Kind;

    PECTPSolution* recentSolution;
    std::stack< std::pair< Kind, std::vector<int> > > logForUndo;

    PECTPDatasetReceiver* dr;
    PECTPDataPreprocessor* dp;

    void addLog(Kind kind, std::vector<int> arg, PECTPSolution* solution){
        if(solution != recentSolution){
            logForUndo = std::stack< std::pair< Kind, std::vector<int> > >();
            recentSolution = solution;
        }
        logForUndo.push(std::make_pair(kind, arg));
    }

    bool moveEventWithoutAddLog(int, int, PECTPSolution*);
    bool swapEventWithoutAddLog(int, int, PECTPSolution*);

    void undoToBeforeME(int, std::pair<int, int>, PECTPSolution*);
    void undoToBeforeSE(int, int, std::pair<int, int>, std::pair<int, int>, 
                        PECTPSolution*);

public:
    PECTPNeighborhoodManager()
    : dr(PECTPDatasetReceiver::getInstance()),
      dp(PECTPDataPreprocessor::getInstance()) {}

    bool undo(void);

    bool moveEvent(int, int, PECTPSolution*);
    bool swapEvent(int, int, PECTPSolution*);
};

#endif

