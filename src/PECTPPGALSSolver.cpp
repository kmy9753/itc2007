#include <ctime>
#include <algorithm>
#include <cstdio>

#include "PECTPPGALSSolver.h"
#include "PECTPDatasetReceiver.h"
#include "PECTPDataPreprocessor.h"
#include "PECTPSolutionFactory.h"
#include "PECTPSolution.h"


using namespace std;

void PECTPPGALSSolver::run(){

    PECTPDatasetReceiver* dr = PECTPDatasetReceiver::getInstance();
    PECTPDataPreprocessor* dp = PECTPDataPreprocessor::getInstance();
    PECTPSolutionFactory sf(0, dr->eventNum / individualsNum);
   
    for(int i = 0; i < individualsNum; i++){
        PECTPSolution* solution = sf.create();

        probGASolver.setIndividual(solution, i);
    }

    clock_t start = clock();

    int preVariance = 0, stableCnt = 0;
    int turnCnt = 0;

    bestSolution = *probGASolver.getIndividual(0);

    while((clock() - start) / CLOCKS_PER_SEC <= 260){

        probGASolver.run();

        double dV = (preVariance != 0) ? (double)probVec.getVariance() / preVariance:1.;
        if(dV < 1.2){
            stableCnt++;
        }
        else stableCnt = 0;
        if(dV < 0.8 || 5 <= stableCnt){
            for(int i = 0; i < individualsNum * 4 / 5; i++){
                PECTPSolution* solution = sf.create();

                probGASolver.setIndividual(solution);
            }
            probVec.reset();
            preVariance = 0;
            stableCnt = 0;

            continue;
        }
        preVariance = probVec.getVariance();
    
        PECTPSolution* _solution = probGASolver.getBestIndividual();
        PECTPSmartSolution solution;
        solution = *_solution;
        probLSSolver.setSolution(&solution);
        probLSSolver.run();

        *_solution = solution;

        if(solution.getHardViolation() + solution.getSoftViolation() + solution.getDistanceToFeasibility() < 
           bestSolution.getHardViolation() + bestSolution.getSoftViolation() + bestSolution.getDistanceToFeasibility()){
            if(bestSolution.getHardViolation() == 0 && solution.getHardViolation() != 0);
            else bestSolution = solution;
        }

    }
    cout << "----------------------------" << endl;
    int distanceToFeasibilityRate = 0;
    for(int e = 0; e < dr->eventNum; e++){
        if(bestSolution.getTimeslot(e) == PECTPSolution::NONE){
            distanceToFeasibilityRate++;
        }
    }
    distanceToFeasibilityRate *= 100;
    distanceToFeasibilityRate /= dr->eventNum;

    printf("%6d %6d %3d %6d\n", bestSolution.getHardViolation(), bestSolution.getSoftViolation(), 100 - distanceToFeasibilityRate, bestSolution.getDistanceToFeasibility());
}

