#include <algorithm>

#include <iostream> //for debug

#include "PECTPViolationCalculator.h"

using namespace std;

void PECTPViolationCalculator::makeSchedule(PECTPSolution& solution){
    curSchedule = vector< vector< vector<int> > >(
            dr->studentNum,
            vector< vector<int> >(
                dr->dayNum, vector<int>(dr->timeslotNumPerDay, 0)
                )
            );

    for(int e = 0; e < dr->eventNum; e++){
        int t = solution.getTimeslot(e);
        if(t < 0 || dr->timeslotNum <= t){
            continue;
        }

        for(int s = 0; s < dr->studentNum; s++){
            if(dr->relationBetSE[s][e]){
                curSchedule[s][t % dr->dayNum][t / dr->dayNum]++;
            }
        }
    }
}

int PECTPViolationCalculator::calculateLateEventsViolation(vector<int>& schedule){
    int ret = 0;

    if(schedule[dr->timeslotNumPerDay - 1]){
        ret = 1;
    }
    
    return ret;
}

int PECTPViolationCalculator::calculateConsecutiveEventsViolation(vector<int>& schedule){
    int ret = 0, cnt = 0;

    for(int t = 0; t < dr->timeslotNumPerDay; t++){
        if(schedule[t]){
            cnt++;
        }
        else{
            cnt = 0;
        }
        if(2 < cnt) ret++;
    }

    return ret;
}

int PECTPViolationCalculator::calculateIsolatedEventsViolation(vector<int>& schedule){
    int ret = 0;

    if(count(schedule.begin(), schedule.end(), 0) == dr->timeslotNumPerDay - 1){
        ret = 1;
    }

    return ret;
}

int PECTPViolationCalculator::calculateSoftViolation(PECTPSolution& solution){
    int s1 = 0, s2 = 0, s3 = 0;

    //make curSchedule
    makeSchedule(solution);

    for(int s = 0; s < dr->studentNum; s++){
        for(int d = 0; d < dr->dayNum; d++){
            s1 += calculateLateEventsViolation(curSchedule[s][d]);
            s2 += calculateConsecutiveEventsViolation(curSchedule[s][d]);
            s3 += calculateIsolatedEventsViolation(curSchedule[s][d]);
        }
    }
//    cout << "more than two in a row : " << s2 << endl;
//    cout << "one class on a day     : " << s3 << endl;
//    cout << "last time slot of a day: " << s1 << endl;

    return s1 + s2 + s3; 
}

int PECTPViolationCalculator::calculateHardViolation(PECTPSolution& solution){
    int ret = 0;

    for(int e1 = 0; e1 < dr->eventNum; e1++){

        //e1 has already violated H6
        if(solution.getTimeslot(e1) == PECTPSolution::NONE)
            continue;

        for(int e2 = e1 + 1; e2 < dr->eventNum; e2++){

            //violate H1
            ret += calculateH1(
                     e1, e2, 
                     solution.getTimeslot(e1), solution.getTimeslot(e2)
                   );

            //violate H5
            ret += calculateH5(
                     e1, e2, 
                     solution.getTimeslot(e1), solution.getTimeslot(e2)
                   );
        }
    }

    return ret;
}

int PECTPViolationCalculator::calculateDistanceToFeasibility(PECTPSolution& solution){
    int ret = 0;

    //violate H6
    for(int e = 0; e < dr->eventNum; e++){
        if(solution.getTimeslot(e) == PECTPSolution::NONE)
            ret += dp->enrolmentedStudentsNum[e];
    }
    
    return ret;
}

int PECTPViolationCalculator::calculateH1(int e1, int e2, int t1, int t2){

    //if violation exists, then ret = deltaRet
    int ret;

    //either e1 or e2 already has violated H6
    if(t1 == PECTPSolution::NONE || 
       t2 == PECTPSolution::NONE)
        ret = 0;

    else if(dp->relationBetEE[e1][e2] ||
            t1 != t2)
        ret = 0;

    else ret = min(dp->enrolmentedStudentsNum[e1], 
                   dp->enrolmentedStudentsNum[e2]);

    return ret;
}

int PECTPViolationCalculator::calculateH5(int e1, int e2, int t1, int t2){

    //if violation exists, then ret = deltaRet
    int ret;

    //either e1 or e2 already has violated H6
    if(t1 == PECTPSolution::NONE || 
       t2 == PECTPSolution::NONE)
        ret = 0;

    else if((dr->relationBetEE[e1][e2] ==  1 && t1 <= t2) ||
            (dr->relationBetEE[e1][e2] == -1 && t2 <= t1))
        ret = min(dp->enrolmentedStudentsNum[e1], 
                  dp->enrolmentedStudentsNum[e2]);

    else ret = 0;

    return ret;
}

