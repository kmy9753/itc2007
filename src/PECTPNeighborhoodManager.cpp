#include <iostream> //forDebug

#include "PECTPNeighborhoodManager.h"

using namespace std;

bool PECTPNeighborhoodManager::moveEventWithoutAddLog(int e, int t, PECTPSolution* solution){
    int r = PECTPSolution::NONE;

    if(t != PECTPSolution::NONE){
        if(!dr->relationBetET[e][t] || dr->roomNum <= solution->countEvent(t)){
            return false;
        }

        if(!dp->allRoomEvent[e] &&
           (r = solution->findRoom(e, t)) == PECTPSolution::NONE){
            return false;
        }
    }
    
    //set to solution
    solution->set(e, make_pair(t, r));

    return true;
}

bool PECTPNeighborhoodManager::moveEvent(int e, int t, PECTPSolution* solution){
    pair<int, int> pairTR = solution->get(e);

    bool ret = moveEventWithoutAddLog(e, t, solution);

    if(ret){
        //add to logForUndo
        vector<int> arg(3);
        arg[0] = e, arg[1] = pairTR.first, arg[2] = pairTR.second;
        addLog(MOVE_EVENT, arg, solution);
    }

    return ret;
}

bool PECTPNeighborhoodManager::swapEventWithoutAddLog(int e1, int e2, PECTPSolution* solution){
    pair<int, int> pairTR1 = solution->get(e1), pairTR2 = solution->get(e2);
    solution->set(e2, make_pair(PECTPSolution::NONE, PECTPSolution::NONE));

    if(moveEventWithoutAddLog(e1, pairTR2.first, solution) &&
       moveEventWithoutAddLog(e2, pairTR1.first, solution)){
        return true;
    }

    undoToBeforeSE(e1, e2, pairTR1, pairTR2, solution);

    return false;
}

bool PECTPNeighborhoodManager::swapEvent(int e1, int e2, PECTPSolution* solution){
    pair<int, int> pairTR1 = solution->get(e1), pairTR2 = solution->get(e2);
    bool ret = swapEventWithoutAddLog(e1, e2, solution);

    if(ret){
        //add to logForUndo
        vector<int> arg(6);
        arg[0] = e1, arg[1] = e2, arg[2] = pairTR1.first, 
        arg[3] = pairTR1.second, arg[4] = pairTR2.first, 
        arg[5] = pairTR2.second;

        addLog(SWAP_EVENT, arg, solution);
    }

    return ret;
}

bool PECTPNeighborhoodManager::undo(void){
    if(logForUndo.empty()) return false;

    Kind kind = logForUndo.top().first;
    vector<int> arg = logForUndo.top().second;

    logForUndo.pop();

    switch(kind){
    case MOVE_EVENT:
    {
        undoToBeforeME(arg[0], make_pair(arg[1], arg[2]), recentSolution);
        break;
    }
    case SWAP_EVENT:
    {
        undoToBeforeSE(arg[0], arg[1], 
                      make_pair(arg[2], arg[3]), make_pair(arg[4], arg[5]), 
                      recentSolution);  
        break;
    }
    default:
        break; 
    }

    return true;
}

void PECTPNeighborhoodManager::undoToBeforeME(int e, pair<int, int> pairTR, PECTPSolution* solution){
    solution->set(e, pairTR);
}

void PECTPNeighborhoodManager::undoToBeforeSE(int e1, int e2, pair<int, int> pairTR1, pair<int, int> pairTR2, PECTPSolution* solution){
    solution->set(e1, pairTR1);
    solution->set(e2, pairTR2);
}
