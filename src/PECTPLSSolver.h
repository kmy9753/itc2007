#include "PECTPDatasetReceiver.h"
#include "PECTPDataPreprocessor.h"
#include "PECTPSmartSolution.h"
#include "PECTPViolationCalculator.h"
#include "PECTPNeighborhoodManager.h"

#ifndef PECTP_LS_SOLVER_H
#define PECTP_LS_SOLVER_H

class PECTPLSSolver{
private:
    PECTPSmartSolution* solution;
    int evaluationValue;
    int curEvent;
    double coefHardViolation;

    static const int DEFAULT_TRIAL_NUM = 3000;

    PECTPDatasetReceiver* dr;
    PECTPDataPreprocessor* dp;
    PECTPNeighborhoodManager neighborhoodManager;

    bool updateSolution(void);

protected:
    virtual int selectTargetEvent(void);

public:
    void setSolution(PECTPSmartSolution* solution){
        this->solution = solution;
        evaluationValue = (int)(coefHardViolation * solution->getHardViolation()) 
                        + solution->getSoftViolation()
                        + solution->getDistanceToFeasibility();
    }

    void run(void);
    void run(int);

    PECTPSolution* getSolution(void){
        return solution;
    }

    PECTPLSSolver() 
    : dr(PECTPDatasetReceiver::getInstance()),
      dp(PECTPDataPreprocessor::getInstance()),
      neighborhoodManager(),
      coefHardViolation(1.)
    {
        curEvent = rand() % dr->eventNum;
    }

    virtual ~PECTPLSSolver(){ }
};

#endif
