#include <vector>

#include "PECTPDatasetReceiver.h"
#include "PECTPDataPreprocessor.h"
#include "PECTPSolution.h"

#ifndef PECTP_VIOLATION_CALCULATOR_H
#define PECTP_VIOLATION_CALCULATOR_H

class PECTPViolationCalculator{
private:
    void makeSchedule(PECTPSolution&);

    //static const int NO_EVENT = -1;

protected:
    std::vector< std::vector< std::vector<int> > > curSchedule;

    PECTPDatasetReceiver* dr;
    PECTPDataPreprocessor* dp;

    int calculateH1(int, int, int, int);
    int calculateH5(int, int, int, int);

    int calculateLateEventsViolation(std::vector<int>&);
    int calculateConsecutiveEventsViolation(std::vector<int>&);
    int calculateIsolatedEventsViolation(std::vector<int>&);

public:
    PECTPViolationCalculator()
    : dr(PECTPDatasetReceiver::getInstance()),
      dp(PECTPDataPreprocessor::getInstance())
    {}

    int calculateSoftViolation(PECTPSolution&);
    int calculateHardViolation(PECTPSolution&);
    int calculateDistanceToFeasibility(PECTPSolution&);

    virtual ~PECTPViolationCalculator(){}
};


#endif
