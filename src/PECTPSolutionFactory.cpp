#include "PECTPSolutionFactory.h"
#include <cstdlib>
#include <set>
#include <vector>

using namespace std;

PECTPSolution* PECTPSolutionFactory::create(void){
    init();

    return createByI1();
}

PECTPSolution* PECTPSolutionFactory::createByI0(void){

    PECTPSolution* instance = new PECTPSolution();

    for(int eCnt = 0, e = defaultEvent; eCnt < dr->eventNum; eCnt++, e = (e + 1) % dr->eventNum){

        //a target timeslot
        int t = PECTPSolution::NONE;

        //until the index is determined
        for(int tCnt = 0, ind = rand() % dp->availableTimeslot[e].size(); tCnt < dp->availableTimeslot[e].size(); tCnt++){

            //an index of availableTimeslot
            ind = (ind + 1) % dp->availableTimeslot[e].size();
            if(instance->countEvent(dp->availableTimeslot[e][ind]) < dr->roomNum){
                t = dp->availableTimeslot[e][ind];
            }
        }

        int r = decideRoom(e, t, instance);

        if(t != PECTPSolution::NONE){
            instance->set(e, make_pair(t, r));
            break;
        }
    }

    return instance;
}

PECTPSolution* PECTPSolutionFactory::createByI1(void){

    PECTPSolution* instance = new PECTPSolution();

    for(int eCnt = 0, e = defaultEvent; eCnt < dr->eventNum; eCnt++, e = (e + 1) % dr->eventNum){

        //a target timeslot
        int t = PECTPSolution::NONE;

        //until the index is determined
        for(int tCnt = 0, ind = rand() % dp->availableTimeslot[e].size(); tCnt < dp->availableTimeslot[e].size(); tCnt++){

            //an index of availableTimeslot
            ind = (ind + 1) % dp->availableTimeslot[e].size();
            if(instance->countEvent(dp->availableTimeslot[e][ind]) < dr->roomNum){
                t = dp->availableTimeslot[e][ind];
            }

            int r = decideRoom(e, t, instance);

            if(t != PECTPSolution::NONE){
                instance->set(e, make_pair(t, r));
                break;
            }
        }
    }

    return instance;
    
}

int PECTPSolutionFactory::decideRoom(int e, int& t, PECTPSolution* solution){

    //a target room
    int r = PECTPSolution::NONE;

    //the index is available AND
    //the event does care about assigned room
    if(t != PECTPSolution::NONE && !dp->allRoomEvent[e]){
        //until the room is determined
        r = solution->findRoom(e, t);

        //the room is NOT found
        if(r == PECTPSolution::NONE) t = PECTPSolution::NONE;
    }

    return r;
}
