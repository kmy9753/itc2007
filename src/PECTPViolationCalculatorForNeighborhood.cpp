#include "PECTPViolationCalculatorForNeighborhood.h"

#include <iostream> //forDebug

using namespace std;

int PECTPViolationCalculatorForNeighborhood::calculateDeltaHardViolationAfterME(int tarE, int preT, int tarT){
    
    if(preT == tarT) return 0;

    int ret = 0;

    if(preT == PECTPSolution::NONE);

    else{
        for(int e = 0; e < dr->eventNum; e++){
            if(e == tarE) continue;
            ret -= calculateH1(tarE, e, preT, solution->getTimeslot(e));
            ret -= calculateH5(tarE, e, preT, solution->getTimeslot(e));
        }
    }
        
    if(tarT == PECTPSolution::NONE);

    else{
        for(int e = 0; e < dr->eventNum; e++){
            if(e == tarE) continue;
            ret += calculateH1(tarE, e, tarT, solution->getTimeslot(e));
            ret += calculateH5(tarE, e, tarT, solution->getTimeslot(e));
        }
    }

    return ret;
}

int PECTPViolationCalculatorForNeighborhood::calculateDeltaDistanceToFeasibilityAfterME(int tarE, int preT, int tarT){
    if(preT == tarT) return 0;

    int ret = 0;

    if(preT == PECTPSolution::NONE)
        ret -= dp->enrolmentedStudentsNum[tarE];
    
    if(tarT == PECTPSolution::NONE)
        ret += dp->enrolmentedStudentsNum[tarE];

    return ret;
}

int PECTPViolationCalculatorForNeighborhood::calculateDeltaSoftViolationAfterME(int tarE, int preT, int tarT){

    if(preT == tarT) return 0;

    int ret = 0;
    int preD = preT % dr->dayNum, tarD = tarT % dr->dayNum;

    for(int s = 0; s < dr->studentNum; s++){
        if(!dr->relationBetSE[s][tarE]) continue;

        if(preT == PECTPSolution::NONE);
        else if(curSchedule[s][preD][preT / dr->dayNum] == 1){
            if(preT / dr->dayNum == dr->timeslotNumPerDay - 1){
                ret--;
            }

            ret -= (calculateConsecutiveEventsViolation(curSchedule[s][preD])
                +   calculateIsolatedEventsViolation(curSchedule[s][preD]));
            curSchedule[s][preD][preT / dr->dayNum]--;
            ret += (calculateConsecutiveEventsViolation(curSchedule[s][preD])
                +   calculateIsolatedEventsViolation(curSchedule[s][preD]));
        }
        else curSchedule[s][preD][preT / dr->dayNum]--;

        if(tarT == PECTPSolution::NONE);
        else if(curSchedule[s][tarD][tarT / dr->dayNum] == 0){
            if(tarT / dr->dayNum == dr->timeslotNumPerDay - 1){
                ret++;
            }

            ret -= (calculateConsecutiveEventsViolation(curSchedule[s][tarD])
                +   calculateIsolatedEventsViolation(curSchedule[s][tarD]));
            curSchedule[s][tarD][tarT / dr->dayNum]++;
            ret += (calculateConsecutiveEventsViolation(curSchedule[s][tarD])
                +   calculateIsolatedEventsViolation(curSchedule[s][tarD]));
        }
        else curSchedule[s][tarD][tarT / dr->dayNum]++;
    }

    return ret;
}

