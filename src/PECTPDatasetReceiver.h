#include <vector>

#ifndef PECTP_DATASET_RECEIVER_H
#define PECTP_DATASET_RECEIVER_H

class PECTPDatasetReceiver{
private:
    PECTPDatasetReceiver()
    : dayNum(5),
      timeslotNumPerDay(9)
    {
        timeslotNum = dayNum * timeslotNumPerDay;
    }
    
    static PECTPDatasetReceiver* instance;

public:
    int eventNum, roomNum, featureNum, studentNum, dayNum, timeslotNumPerDay, timeslotNum; 
    std::vector<int> roomCap;
    std::vector< std::vector<bool> > relationBetSE, relationBetRF, relationBetEF, relationBetET;
    std::vector< std::vector<int> > relationBetEE;
    
    void receive(void);
    
    static PECTPDatasetReceiver* getInstance(void){
        if(instance == 0){
            instance = new PECTPDatasetReceiver();
            instance->receive();
        }

        return instance;
    }
};

#endif
