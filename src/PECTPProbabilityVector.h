#include <vector>
#include <cstdlib>
#include <cmath>

#include "PECTPDatasetReceiver.h" 

#ifndef PECTP_PROBABILITY_VECTOR_H
#define PECTP_PROBABILITY_VECTOR_H

class PECTPProbabilityVector{
private:
    int totalAppearance;
    std::vector<int> histgram;

    PECTPDatasetReceiver* dr;

public:
    void reset(void){
        totalAppearance = 0;
        histgram = std::vector<int>(dr->eventNum, 0);
    }
    void increment(int e){
        histgram[e]++;
    }
    int getEvent(int probability){
        probability %= totalAppearance;
        
        int cnt = 0;
        for(int e = 0; e < dr->eventNum; e++){
            cnt += histgram[e];
            if(probability <= cnt){
                return e;
            }
        }
    }
    int getEvent(void){
        getEvent(rand());
    }
    int size(void){
        return histgram.size();
    }
    void setValue(int e, int val){
        histgram[e] = val;
    }
    int getVariance(void){
        double m = 0, v = 0;

        for(int e = 0; e < dr->eventNum; e++){
            m += histgram[m] / (double)dr->eventNum;
        }
        for(int e = 0; e < dr->eventNum; e++){
            v += pow((double)(histgram[m] - m), 2.) / dr->eventNum;
        }
        
        return (int)v;
    }

    PECTPProbabilityVector()
    : dr(PECTPDatasetReceiver::getInstance()){
        reset();
    }
};

#endif

