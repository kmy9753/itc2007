#include "PECTPProbabilisticGASolver.h"
#include "PECTPProbabilisticLSSolver.h"
#include "PECTPProbabilityVector.h"
#include "PECTPSmartSolution.h"

#ifndef PECTP_PGALS_SOLVER
#define PECTP_PGALS_SOLVER

class PECTPPGALSSolver{
private:
    PECTPProbabilisticGASolver probGASolver;
    PECTPProbabilisticLSSolver probLSSolver;

    PECTPProbabilityVector probVec;

    int individualsNum;

    PECTPSmartSolution bestSolution;

public:
    PECTPPGALSSolver(int individualsNum)
    : probVec(),
      probGASolver(&probVec, individualsNum),
      probLSSolver(&probVec){
        this->individualsNum = individualsNum;
        
        //probGASolver = PECTPProbabilisticGASolver(&probVec, individualsNum);
        //probLSSolver = PECTPProbabilisticLSSolver(&probVec);
    }

    void run(void);
};

#endif

