#include <vector>
#include <utility>

#include "PECTPDatasetReceiver.h"
#include "PECTPDataPreprocessor.h"

#ifndef PECTP_SOLUTION_H
#define PECTP_SOLUTION_H

class PECTPSolution{
private:
    std::vector< std::pair<int, int> > solution;

    std::vector<int> eventCnt;
    std::vector< std::vector<bool> > assignedRoom;
    PECTPDataPreprocessor* dp;

protected:
    PECTPDatasetReceiver* dr;

public:
    static const int NONE = -1;

    PECTPSolution()
    : dr(PECTPDatasetReceiver::getInstance()),
      dp(PECTPDataPreprocessor::getInstance()){
        solution = std::vector< std::pair<int, int> >(dr->eventNum, std::make_pair(NONE, NONE));
        eventCnt = std::vector<int>(dr->timeslotNum);
        assignedRoom = std::vector< std::vector<bool> >(dr->timeslotNum, std::vector<bool>(dr->roomNum, false));
    }

    int getTimeslot(int e) const{
        return solution[e].first;
    }
    int getRoom(int e) const{
        return solution[e].second;
    }
    std::pair<int, int> get(int e) const{
        return solution[e];
    }
    virtual void set(int e, std::pair<int, int> pairTR){
        int nextT = pairTR.first, nextR = pairTR.second;

        if(((nextT < 0 || dr->timeslotNum <= nextT) && nextT != NONE) || 
           ((nextR < 0 || dr->roomNum <= nextR) && nextR != NONE))
            return;

        int preT = getTimeslot(e), preR = getRoom(e);

        solution[e].first = nextT;
        solution[e].second = nextR;

        if(preT != NONE)
            eventCnt[preT]--;
        if(nextT != NONE)
            eventCnt[nextT]++;

        if(0 <= preT && preT < dr->timeslotNum && preR != PECTPSolution::NONE)
            assignedRoom[preT][preR] = false;
        if(0 <= nextT && nextT < dr->timeslotNum && nextR != PECTPSolution::NONE)
            assignedRoom[nextT][nextR] = true;
    }
    int size(){
        return solution.size();
    }
    int countEvent(int t){
        return eventCnt[t];
    }
    bool isAssignedRoom(int t, int r){
        return assignedRoom[t][r];
    }
    int findRoom(int e, int t){
        for(int i = 0; i < dp->roomCharmOrder.size(); i++){
            int r = dp->roomCharmOrder[i];
            if(dp->relationBetER[e][r] && !isAssignedRoom(t, r))
                return r;
        }
        return NONE;
    }

    virtual ~PECTPSolution(){};
};

#endif
