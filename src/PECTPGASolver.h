#include "PECTPDatasetReceiver.h"
#include "PECTPDataPreprocessor.h"
#include "PECTPSolution.h"

#ifndef PECTP_GA_SOLVER_H
#define PECTP_GA_SOLVER_H

class PECTPGASolver{
private:
    std::vector<PECTPSolution*> individuals;

    std::vector<bool> assigned;

    double crossoverProbability;
    double mutationProbability;

    static const double DEFAULT_CROSSOVER_PROBABILITY = 0.7;
    static const double DEFAULT_MUTATION_PROBABILITY = 0.005;
    static const int DEFAULT_TOURNAMENT_SIZE = 2;
    static const int DEFAULT_TRIAL_NUM = 20;

    PECTPDataPreprocessor* dp;

    PECTPSolution* crossover(PECTPSolution*, PECTPSolution*);
    PECTPSolution* select(void);
    int selectBadIndividualIndex(void);
    void mutate(PECTPSolution*);

    void crossoverBetOneRoomEvents(PECTPSolution*, PECTPSolution*, PECTPSolution*);
    void crossoverBetRegularRoomEvents(PECTPSolution*, PECTPSolution*, PECTPSolution*);
    void crossoverBetAllRoomEvents(PECTPSolution*, PECTPSolution*, PECTPSolution*);
    void crossoverBetNotAllRoomEvent(PECTPSolution*, PECTPSolution*, PECTPSolution*, int);

    PECTPSolution* selectWithTournamentSelection(int);

protected:
    static const int DEFAULT_INDIVIDUALS_NUM = 10;

    virtual void runPreprocessOfCrossover(PECTPSolution* p1, PECTPSolution* p2);
    PECTPDatasetReceiver* dr;

public:
    void setIndividual(PECTPSolution*, int);    //set the individual at the index
    void setIndividual(PECTPSolution*);         //an index will be selected in this function

    void run(void);
    void run(int);

    PECTPSolution* getIndividual(int);          //get the individual at the index
    PECTPSolution* getBestIndividual(void);
    PECTPSolution* getBadIndividual(void);

    std::vector<int> fitnesses;

    PECTPGASolver(int individualsNum = DEFAULT_INDIVIDUALS_NUM)
    : dr(PECTPDatasetReceiver::getInstance()),
      dp(PECTPDataPreprocessor::getInstance()),
      crossoverProbability(DEFAULT_CROSSOVER_PROBABILITY),
      mutationProbability(DEFAULT_MUTATION_PROBABILITY),
      individuals(individualsNum),
      fitnesses(individualsNum){
        assigned = std::vector<bool>(dr->eventNum);
    }
    
    virtual ~PECTPGASolver(){}
};

#endif//
