#include <iostream> //forDebug

#include "PECTPSolution.h"
#include "PECTPViolationCalculatorForNeighborhood.h"

#ifndef PECTP_SMART_SOLUTION_H
#define PECTP_SMART_SOLUTION_H

class PECTPSmartSolution : public PECTPSolution{
private:
    PECTPViolationCalculatorForNeighborhood calculator;
    int hardViolation, softViolation, distanceToFeasibility;

    void init(void){
        hardViolation = calculator.calculateHardViolation(*dynamic_cast<PECTPSolution*>(this));
        softViolation = calculator.calculateSoftViolation(*dynamic_cast<PECTPSolution*>(this));
        distanceToFeasibility = calculator.calculateDistanceToFeasibility(*dynamic_cast<PECTPSolution*>(this));
    }

public:
    PECTPSmartSolution()
    : calculator(dynamic_cast<PECTPSolution*>(this)){
        init();
    }

    PECTPSmartSolution &operator=(const PECTPSolution &obj){
        for(int e = 0; e < dr->eventNum; e++){
            PECTPSolution::set(e, obj.get(e));
        }
        init();
    }
    void set(int e, std::pair<int, int> pairTR){
        int preT = getTimeslot(e);
        PECTPSolution::set(e, pairTR);

        hardViolation += calculator.calculateDeltaHardViolationAfterME(e, preT, pairTR.first);
        softViolation += calculator.calculateDeltaSoftViolationAfterME(e, preT, pairTR.first);
        distanceToFeasibility += calculator.calculateDeltaDistanceToFeasibilityAfterME(e, preT, pairTR.first);
    }
    int getHardViolation(void){
        return hardViolation;
    }
    int getSoftViolation(void){
        return softViolation;
    }
    int getDistanceToFeasibility(void){
        return distanceToFeasibility;
    }

};

#endif
