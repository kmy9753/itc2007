#include <iostream>
#include <vector>
#include <algorithm> 
#include <cstdlib>
#include <ctime>
#include <cstdio>

#include "PECTPDatasetReceiver.h"
#include "PECTPDataPreprocessor.h"
#include "PECTPSolutionFactory.h"
#include "PECTPSolution.h"
#include "PECTPGASolver.h"
#include "PECTPViolationCalculator.h"
#include "PECTPLSSolver.h"

#include "PECTPProbabilityVector.h"
#include "PECTPProbabilisticGASolver.h"
#include "PECTPProbabilisticLSSolver.h"
#include "PECTPPGALSSolver.h"

using namespace std;

int main(void){
//int PPGALS(void){
    srand((unsigned)time(NULL));
    PECTPPGALSSolver solver(20);

    solver.run();

    return 0;
}

//int main(void){
int GA(void){
    srand((unsigned)time(NULL));
    const int N = 10;

    //initialize seed
    srand((unsigned)time(NULL));

    PECTPDatasetReceiver* dr = PECTPDatasetReceiver::getInstance();
    PECTPSolutionFactory sf(0, 40);
    PECTPViolationCalculator vc;
    PECTPProbabilityVector pv;
    PECTPProbabilisticGASolver gaSolver(&pv, N);

    for(int i = 0; i < N; i++){
        PECTPSolution* solution = sf.create();

        for(int e = 0; e < dr->eventNum; e++){
//            cout << e << " " << solution->getTimeslot(e)  << 
//                " " << solution->getRoom(e) << endl;
        }
        gaSolver.setIndividual(solution, i);
    }

    for(int i = 0; i < N; i++){
        PECTPSolution* solution = gaSolver.getIndividual(i);
        cout << "index = " << i << " Before::Hard = " << vc.calculateHardViolation(*solution) <<  " Soft = " << vc.calculateSoftViolation(*solution) << endl;
    }
    
    for(int i = 0; i < 1000; i++){
        gaSolver.run(20);
        for(int i = 0; i < N; i++){
            PECTPSolution* solution = gaSolver.getIndividual(i);
            cout << "index = " << i << " Before::Hard = " << vc.calculateHardViolation(*solution) <<  " Soft = " << vc.calculateSoftViolation(*solution) << endl;
        }
    }
    return 0;
}

//int main(void){
int LS(void){
    srand((unsigned)time(NULL));

    clock_t start = clock();
    PECTPDatasetReceiver* dr = PECTPDatasetReceiver::getInstance();
    PECTPDataPreprocessor* dp = PECTPDataPreprocessor::getInstance();
    //PECTPProbabilisticLSSolver lsSolver(&pv);
    PECTPLSSolver lsSolver;
    PECTPSolutionFactory sf(0, 40);
    PECTPSolution* _solution = sf.create();
    PECTPSmartSolution solution;
    solution = *_solution;
    lsSolver.setSolution(&solution);

    PECTPViolationCalculator vc;

    PECTPSmartSolution bestSolution;
    bestSolution = solution;

    while((clock() - start) / CLOCKS_PER_SEC <= 260){
        lsSolver.run(100);
        
        if(solution.getHardViolation() + solution.getSoftViolation() + solution.getDistanceToFeasibility() < 
           bestSolution.getHardViolation() + bestSolution.getSoftViolation() + bestSolution.getDistanceToFeasibility()){
            if(bestSolution.getHardViolation() == 0 && solution.getHardViolation() != 0);
            else bestSolution = solution;
        }
    }

    cout << "----------------------------" << endl;
    int distanceToFeasibilityRate = 0;
    for(int e = 0; e < dr->eventNum; e++){
        if(bestSolution.getTimeslot(e) == PECTPSolution::NONE){
            distanceToFeasibilityRate++;
        }
    }
    distanceToFeasibilityRate *= 100;
    distanceToFeasibilityRate /= dr->eventNum;

    printf("%6d %6d %3d %6d\n", bestSolution.getHardViolation(), bestSolution.getSoftViolation(), 100 - distanceToFeasibilityRate, bestSolution.getDistanceToFeasibility());

    return 0;
}

